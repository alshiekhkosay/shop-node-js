const mysql = require("mysql2");

const pool = mysql.createPool({
    host:"localhost",
    user:"root",
    database:"shop-nodeJs",
})

module.exports = pool.promise();
